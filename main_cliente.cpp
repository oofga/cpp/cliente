#include "cliente.hpp"
#include <iomanip>
#include <iostream>

using namespace std;

int main(int argc, char ** argv){

    Cliente cliente1;

    cliente1.set_nome("Douglas");
    cliente1.set_cpf(234234244234);
    cliente1.set_telefone("77678-3455");
    cliente1.set_email("douglas@gmail.com");
    cliente1.set_data_de_cadastro("01/07/2010");
    cliente1.set_total_de_compras(5543.12f);

    cliente1.imprime_dados();

    return 0;
}





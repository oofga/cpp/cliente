#include "cliente.hpp"
#include <iostream>

Cliente::Cliente(){
    set_nome("");
    set_cpf(0);
    set_telefone("");
    set_email("email.com");
    set_data_de_cadastro("");
    set_total_de_compras(0.0f);
    std::cout << "Construtor da classe Cliente" << std::endl; 
}
Cliente::~Cliente(){
    std::cout << "Destrutor da classe Cliente"<< std::endl;
}
void Cliente::set_data_de_cadastro(string data_de_cadastro){
    this->data_de_cadastro = data_de_cadastro;
}
string Cliente::get_data_de_cadastro(){
    return data_de_cadastro;
}
void Cliente::set_total_de_compras(float total_de_compras){
    this->total_de_compras = total_de_compras;
}
float Cliente::get_total_de_compras(){
    return total_de_compras;
}

void Cliente::imprime_dados(){
    cout << "Nome: " << get_nome() << endl;
    cout << "CPF: " << get_cpf() << endl;
    cout << "Telefone: " << get_telefone() << endl;    
    cout << "Email: " << get_email() << endl;
    cout << "Data de Cadastro: " << get_data_de_cadastro() << endl;
    cout << "Total de Compras: " << get_total_de_compras() << endl;
} 






